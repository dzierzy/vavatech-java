package pl.vavatech.travel;

public interface Transportation {

    void transport(String passenger);

}
