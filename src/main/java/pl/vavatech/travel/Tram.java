package pl.vavatech.travel;

public class Tram implements Transportation {
    @Override
    public void transport(String passenger) {
        System.out.println("passenger " + passenger + " is traveling by tram");
    }
}
