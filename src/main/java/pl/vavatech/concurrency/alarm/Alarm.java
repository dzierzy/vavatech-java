package pl.vavatech.concurrency.alarm;


import pl.vavatech.concurrency.ThreadNamePrefixPrintStream;

public class Alarm {

    public static void main(String[] args) {

        System.setOut(new ThreadNamePrefixPrintStream(System.out));



        Beeper b = new Beeper();
        Light l = new Light();

      /*  b.beep();
        l.light();
*/

        Thread t1 = new Thread(b);
        Thread t2 = new Thread(l);
        t1.start();
        t2.start();

        System.out.println("done.");
    }
}
