package pl.vavatech.concurrency.account;

public class Withdraw implements Runnable {

    Account account;

    public Withdraw(Account account) {
        this.account = account;
    }

    @Override
    public void run() {
        for (int i = 0; i < 1_000_000; i++) {
            account.withdraw(1);
        }

        System.out.println("balance after withdraw=" + account.getBalance());


    }
}
