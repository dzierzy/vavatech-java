package pl.vavatech.concurrency.account;

import pl.vavatech.concurrency.ThreadNamePrefixPrintStream;


public class ThreadsStart {

    public static void main(String[] args) {

        System.setOut(new ThreadNamePrefixPrintStream(System.out));

        System.out.println("ThreadsStart.main");
        Account account = new Account();


        Thread t1 = new Thread(new Deposit(account));
        Thread t2 = new Thread(new Withdraw(account));

        t1.start();
        t2.start();



        System.out.println("done.");


    }

}
