package pl.vavatech.concurrency.account;

public class Deposit implements Runnable {

    private Account account;

    public Deposit(Account account) {
        this.account = account;
    }

    @Override
    public void run() {
        for (int i = 0; i < 1_000_000; i++) {
            account.deposit(1);
        }
        System.out.println("balance after deposit=" + account.getBalance());

    }
}
