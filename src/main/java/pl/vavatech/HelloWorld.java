package pl.vavatech;

import pl.vavatech.calc.Calculator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HelloWorld {


    public static void main(String[] args){ // psvm
        System.out.println("Hello Everyone!"); // sout
        
        int age = 32;
        System.out.println("age = " + age);

        int[] weekendDays = {5,6,7};
        int weekDay = args.length>0 ? Integer.parseInt(args[0]) : 7;

        boolean weekend = false;

     /*   for(int i=0;i<weekendDays.length;i++){
            if(weekendDays[i]==weekDay){
                weekend = true;
            }
        }*/

        for( int day : weekendDays){ // foreach
            if(day==weekDay){
                weekend = true;
                break;
            }
        }

        if(weekend){
            System.out.println("Yupi!!");
        } else {
            System.out.println("Still waiting :(");
        }

        char c = '\r';
        System.out.println("c = " + (short)c); // cast

        age = age * age;
        age *= age;

        int size = args.length>0 ? Integer.parseInt(args[0]) : 10;
        int[][] results = new int[size][size];

        for(int i=0; i<size; i++){
            for(int j=0; j<size; j++){
                results[i][j] =
                        Calculator.multiply(i+1, j+1); //(i+1)*(j+1);
            }
        }

        for( int[] row : results){
            for( int value : row){
                System.out.print(value + "\t");
            }
            System.out.println();
        }

        System.out.println("done.");

        String s1 = "Hello World!";
        String s2 = new String("Hello World!   ");
        String s3 = "Hello World!";
        System.out.println(s1.equals(s2));
        System.out.println(s2.equals(s3));
        System.out.println(s1.equals(s3));


        s1 = s1.toUpperCase();
        //s1 = s1.replace(" WORLD!", "");
        s1 = s1.substring(0, 5); // HELLO WORLD!

        s1 = s1.concat(s2).trim(); // s1 + s2
        System.out.println("s1 = " + s1);

        int index = s1.lastIndexOf('l');
        System.out.println("l index=" + index);

        System.out.println("s1 lenght=" + s1.length());

        String[] strings = {"a", "b", "c", "d", "e", "f", "g"};

        String all = join(strings);
        System.out.println("all = " + all);

        Pattern p = Pattern.compile("\\d\\d[-\\s]?\\d\\d\\d[$\\s]");
        Matcher m = p.matcher("my address 12-345 or 12 345 or 12345 or 123456");
        while (m.find()) {
            System.out.println(m.start() + " " + m.end() + " " + m.group());

        }

    }
    
    public static String join(String[] strings){
        //String temp = "";
        StringBuilder sb = new StringBuilder();
        for( String element : strings){
            //temp = temp + element; // +1 object
            sb.append(element);
        }
        return sb.toString();
    }

    // Garbage Collector - odsmiecacz



}
