package pl.vavatech.zoo;

import pl.vavatech.travel.Transportation;

public class Horse extends Animal implements Transportation {

    public Horse(int size, String name) {
        super(size, name);
    }

    @Override
    public void move() {
        System.out.println("horse is running");
    }

    @Override
    public void transport(String passenger) {
        System.out.println("passenger " + passenger + " is traveling by horse");
        move();
    }
}
