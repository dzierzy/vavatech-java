package pl.vavatech.zoo;

public abstract class Animal {

    private int size;

    private String name;

    public Animal(int size, String name) {
        this.size = size;
        this.name = name;
    }

    public abstract void move();


    public int getSize() {
        return size;
    }

    public String getName() {
        return name;
    }


    @Override
    public String toString() {
        return "Animal{" +
                "type=" + this.getClass().getName() +
                ", size=" + size +
                ", name='" + name + '\'' +
                '}';
    }
}
