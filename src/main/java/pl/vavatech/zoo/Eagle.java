package pl.vavatech.zoo;

public class Eagle extends Animal {

    public Eagle(int size, String name) {
        super(size, name);
    }

    @Override
    public void move() {
        System.out.println("eagle is flying");
    }
}
