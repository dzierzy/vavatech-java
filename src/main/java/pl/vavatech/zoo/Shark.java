package pl.vavatech.zoo;

public class Shark extends Animal {
    public Shark(int size, String name) {
        super(size, name);
    }

    @Override
    public void move() {
        System.out.println("shark is swimming...");
    }
}
