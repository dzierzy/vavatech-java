package pl.vavatech.zoo;

public class Tiger extends Animal {

    public Tiger(int size, String name) {
        super(size, name);
    }

    @Override
    public void move() {
        System.out.println("tiger is walking");
    }
}
