package pl.vavatech.zoo;

import java.util.*;

public class ZooStarter {

    public static void main(String[] args) {
        System.out.println("ZooStarter.main");

        Animal a = new Horse(50, "Jerry");
        System.out.println("animal: " + a);

        a.move();

        Tiger tiger = new Tiger(100, "Stasiek");
        Animal[] animals = new Animal[100];
        animals[0] = tiger;
        animals[1] = new Horse(150, "Piorun");

        List<Animal> animalList = new ArrayList<>();
        animalList.add(tiger);
        animalList.add(new Eagle(15, "Bielik"));
        animalList.add(new Shark(150, "Jerry"));
        animalList.add(new Horse(200, "Blyskawica"));
        animalList.add(tiger);

        System.out.println("animals count: " + animalList.size());

        AnimalComparator comparator = new AnimalComparator(false);
        Collections.sort(animalList, comparator);

        for( Animal animal : animalList){
            System.out.println("animal: " + animal);
        }

        Map<String, Animal> m = new HashMap<>();

        m.put("Krzysztof", tiger);
        m.put("Adam", new Eagle(12, "Bielik"));
        m.put("Jan", new Shark(99, "Jerry"));

        Animal janAnimal = m.get("Jan");
        System.out.println("janAnimal = " + janAnimal);

        for( Map.Entry<String, Animal> entry : m.entrySet() ){
            System.out.println("entry. key: " + entry.getKey() + ", value: " + entry.getValue());
        }



        System.out.println("done.");
    }

}
