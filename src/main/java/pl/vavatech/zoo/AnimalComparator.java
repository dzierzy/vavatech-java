package pl.vavatech.zoo;

import java.util.Comparator;

public class AnimalComparator implements Comparator<Animal> {

    private boolean ascending = true;

    public AnimalComparator(boolean ascending) {
        this.ascending = ascending;
    }

    @Override
    public int compare(Animal a1, Animal a2) {

        return ascending ?
                a1.getName().compareTo(a2.getName()) :
                a2.getName().compareTo(a1.getName());
    }
}
