package pl.vavatech.calc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class CalculatorStarter {

    public static void main(String[] args)  {
        System.out.println("CalculatorStarter.main");

        System.out.println("2*3=" + Calculator.multiply(2,3));

        double result = 0;
        // try-with-resources
        try(Connection connection = DBUtils.getConnection();) {
            PreparedStatement pStmt = connection.prepareStatement("select result from memory order by id desc limit 1");
            ResultSet rs = pStmt.executeQuery();

            if(rs.next()){
                 result = rs.getDouble("result");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        Calculator c = new Calculator(result); // upcasting
        ScienceCalculator c2 = new ScienceCalculator();


        c.add(3);
        c.multiply(4);
        try {
            c.divide(1);
        } catch (CalculatorException e) {
            System.out.println("Exception:" + e.getMessage());
        }
        //c2.subtract(0.5);
        //c.result = 3.14;

        if(c instanceof ScienceCalculator) {
            ((ScienceCalculator) c).power(3);
        }

        System.out.println("c=" + c);
        // TODO save current calculator result to database

        try(Connection connection = DBUtils.getConnection()){
            connection.setAutoCommit(false);
            //connection.setTransactionIsolation(Connection.TRANSACTION_READ_UNCOMMITTED);
            PreparedStatement stmt = connection.prepareStatement(
                    "INSERT INTO MEMORY(RESULT, CREATED) VALUES(?, ?)");
            stmt.setDouble(1, c.getResult());
            stmt.setDate(2, new java.sql.Date(new Date().getTime()));
            int rows = stmt.executeUpdate();
            System.out.println(rows + " row(s) updated");

            connection.commit();

        } catch (SQLException e) {
            e.printStackTrace();
        }


        c2.add(2);
        c2.power(4);
        //c2.divide(1.1);

        Locale locale = new Locale("en", "GB");
        Date date = new Date(); // 1/1/1970 GMT -> Epoch time
        DateFormat format = new SimpleDateFormat("yyyy~MM~dd", locale);
                // DateFormat.getDateTimeInstance( DateFormat.FULL, DateFormat.FULL, locale );
        String timestamp = format.format(date);

        NumberFormat nf = NumberFormat.getCurrencyInstance(locale);
        nf.setMaximumFractionDigits(7);
        nf.setMinimumFractionDigits(3);
        nf.setMinimumIntegerDigits(5);
        System.out.println(timestamp + " c2=" + nf.format(c2.getResult()));

        String dateString = "2020~04~01";
        Date myDate = null;
        try {
            myDate = format.parse(dateString);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(myDate);
            calendar.add(Calendar.DATE, 30);
            myDate = calendar.getTime();
            System.out.println("myDate = " + myDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }



        Locale pl = new Locale("pl", "PL");
        System.out.println("language=" + locale.getDisplayLanguage(pl) + ", country=" + locale.getDisplayCountry(pl));

    }

}

