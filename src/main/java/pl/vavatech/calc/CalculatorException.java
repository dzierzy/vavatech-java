package pl.vavatech.calc;

public class CalculatorException extends Exception {

    public CalculatorException(String message) {
        super(message);
    }
}
