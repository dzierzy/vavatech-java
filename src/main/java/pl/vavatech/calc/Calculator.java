package pl.vavatech.calc;

public class Calculator /*extends Object*/ {

    // fields
    protected double result;

    // constructors
    public Calculator(double result) {
        this.result = result;
        System.out.println("Calculator parametrized constructor");
    }

    public Calculator(){
        System.out.println("Calculator default constructor");
    }

    // business methods
    public void add(double operand){
        result += operand;
    }

    public void subtract(double operand){
        result -= operand;
    }

    public void multiply(double operand){
        result *= operand;
    }

    public void divide(double operand) throws CalculatorException {
        if(operand==0){
            throw new CalculatorException("pamietaj cholero nie dziel przez zero");
        }
        result /= operand;
    }

    // technical methods
    public double getResult() {
        return this.result;
    }

    public void setResult(double result) {
        this.result = result;
    }

    @Override
    public String toString() {

        return  "Calculator{" +
                "result=" + result +
                '}';
    }

    // static methods
    public static int multiply(int x, int... y){
        int z = x;
        for(int v : y){
            z = z*v;
        }

        return z;
    }

}

