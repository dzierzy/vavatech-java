package pl.vavatech.calc;

/**
 * Scientific Extension of basic calculator class.
 */
public class ScienceCalculator extends Calculator {

    public ScienceCalculator(){
        super(0);
        System.out.println("ScienceCalculator constructor");
    }

    /**
     * implements power function. the result remain in an object state
     * @param p exponent
     */
    public void power(int p){
        /*double temp = result; // result:2 do p:4 -> 2 * 2 * 2 * 2
        for(int i=1; i<p; i++){
            result = result * temp;
        }*/
        result = Math.pow(result, p);
    }


}
