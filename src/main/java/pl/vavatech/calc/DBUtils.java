package pl.vavatech.calc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBUtils {

    /**
     *
     * @return connection to database or null if connection failed
     */
    public static Connection getConnection(){

        String driverName = "org.h2.Driver";
        String url = "jdbc:h2:tcp://localhost/~/test";
        String user = "sa";
        String password = "";

        try {
            Class.forName(driverName);
            return DriverManager.getConnection(url, user, password);

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

}
